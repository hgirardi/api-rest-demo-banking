# Banking Demo APP

This application seeks to respond to the requirements set forth in the technical evaluation for ** @Prestamos Prisma **

### Requirements
The coding challenge task is to create a backend for a simple "banking" application:

1. Client should be able to **sign up** with email & Password
2. Client should be able to **deposit** money
3. Client should be able to **withdraw** money
4. Client should be able to **see the account balance and statement**

Technology stack:

 - Java 8
 - Spring Boot
 - in-memory database H2
 - Maven as build system


### Development
The application implements the following concepts and modules:

**Security** 

 - ``Spring Security`` with basic authentication and user configuration in memory for simplicity.

**REST services**

  - GET `<host>`:8085/account/balance
    - List the account status.
   
  - GET `<host>`:8085/account/statement
    - List the account movements sorted by decreasing date.

  - POST `<host>`:8085/account/deposit/`{amount}`
    - Deposit the amount in the account, updating the account balance to balance + amount.


  - POST `<host>`:8085/account/withdrawal/`{amount}`
    - Subtracts the account balance amount, updating the balance sheet balance - amount.
    
**Business restrictions** *(for validation purposes)*

 - Deposits of `€0` amount are not allowed.
 - The **minimum deposit** is `€10`.
 - The **maximum deposit** per operation is `€3000`.
 - The account balance is *not allowed to be Negative* `(<0)`.

**Error and exception handling**

 - General exceptions handler for controllers.
 - Treatment of customized exceptions.

**Test**

 - Unit testing of `@services`
 - Integral testing of REST services `(@RestController)`


**Database Considerations **

The application has two possible execution profiles (`@Profile`) **"test"** and **"!test"** *(or demo mode)* which complete a database with different preloaded values.

In both cases the structure of the database will be as follows:

Table **CUSTOMER**

 - ID `SERIAL Not null PK`
 - USERNAME `Varchar(50) Not null`
 - PASSWORD `Varchar(20) Not null`
 
 
Table **ACCOUNT**

 - ID `SERIAL Not null PK`
 - BALANCE `Float Not null`
 - OWNER `Numeric Not null FK (CUSTOMER.ID)`
 
Table **ACCOUNT_TRANSACTION**

 - ID `SERIAL Not null PK`
 - TYPE `TinyInt Not null`
 - AMOUNT `Float Not null`
 - DATE `Date Not null`
 - ACCOUNT `Numeric Not null FK (ACCOUNT.ID)`
 
 
### Author
 **Hernán Girardi** [h_girardi@hotmail.com](h_girardi@hotmail.com):
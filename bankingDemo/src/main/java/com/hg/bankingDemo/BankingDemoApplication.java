package com.hg.bankingDemo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.services.IAccountService;
import com.hg.bankingDemo.services.ICustomerService;

/**
 * 
 * @author hernangirardi
 *
 */
@SpringBootApplication
@ComponentScan
public class BankingDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankingDemoApplication.class, args);
	}
	
	@Profile("!test")
	@Bean
	CommandLineRunner initDemoDatabase(ICustomerService customerService, IAccountService accountServices) {
	        return args -> {
	        	//create two demo Customer
	        	Customer customer1 = customerService.save(new Customer(1L,"h_girardi@hotmail.com","her123nan"));
	        	Customer customer2 = customerService.save(new Customer(2L,"other@gmail.com","other"));
	        	
	        	//create demo accounts and link them to demo Customers
	        	accountServices.save(new Account(1L,500,customer1));
	        	accountServices.save(new Account(2L,1000.0,customer2));
	        	
	        	System.out.println("[BEGIN POPULATE DATABASE whit DEMO DATA]");
	        	System.out.println("Customer 1: -> "  + customerService.getCustomerById(1L).toString());
	        	System.out.println("Customer 2: -> "  + customerService.getCustomerById(2L).toString());
	        	System.out.println("Account A: -> "  + accountServices.getAccountByCustomer(customer1).toString());
	        	System.out.println("Account B: -> "  + accountServices.getAccountByCustomer(customer2).toString());
	        	
	        	System.out.println("[END POPULATE DATABASE]");
	        };
	 }
	 
	 @Profile("test")
	 @Bean
	 CommandLineRunner initTestDatabase(ICustomerService customerService, IAccountService accountServices) {
	        return args -> {
	        	//create two demo Customer
	        	Customer customer1 = customerService.save(new Customer(1L,"h_girardi@hotmail.com","her123nan"));
	        	Customer customer2 = customerService.save(new Customer(2L,"other@gmail.com","other"));
	        	
	        	//create demo accounts and link them to demo Customers
	        	accountServices.save(new Account(1L,0,customer1));
	        	accountServices.save(new Account(2L,1000.0,customer2));
	        	
	        	System.out.println("[BEGIN POPULATE TEST DATABASE whit DEMO DATA]");
	        	System.out.println("Customer 1: -> "  + customerService.getCustomerById(1L).toString());
	        	System.out.println("Customer 2: -> "  + customerService.getCustomerById(2L).toString());
	        	
	        	System.out.println("Account A: -> "  + accountServices.getAccountByCustomer(customer1).toString());
	        	System.out.println("Account B: -> "  + accountServices.getAccountByCustomer(customer2).toString());
	        	
	        	System.out.println("[END POPULATE TEST DATABASE]");
	        };
	 }
}

package com.hg.bankingDemo.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * 
 * @author hernangirardi
 *
 */
@Entity
public class Account implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private double balance;
	
	@OneToOne
	@JoinColumn(name="customerOwner",nullable=false)
	private Customer owner;
	
	public Account() {
	}
	
	public Account(Long id, double balance, Customer owner) {
		super();
		this.id = id;
		this.balance = balance;
		this.owner = owner;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public Customer getOwner() {
		return owner;
	}
	public void setOwner(Customer owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", balance=" + balance + ", owner=" + owner.getId() + "]";
	}	
}

package com.hg.bankingDemo.repo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.AccountTransaction;

/**
 * 
 * @author hernangirardi
 *
 */
@Repository
public interface IAccountTransactionRepository extends CrudRepository<AccountTransaction, Long> {
	
	public List<AccountTransaction> findByDateBetweenAndType(LocalDateTime dateStart, LocalDateTime dateEnd, int operationType);
	
	public List<AccountTransaction> findByAccountOrderByDateDesc(Account account);
	
	public Optional<AccountTransaction> findById(Long IdTransaction);
}

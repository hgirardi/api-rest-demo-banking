package com.hg.bankingDemo.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hg.bankingDemo.entity.Customer;

/**
 * 
 * @author hernangirardi
 *
 */
@Repository
public interface ICustomerRepository extends CrudRepository<Customer, Long> {
	
	public Customer findByUsername(String username);

}

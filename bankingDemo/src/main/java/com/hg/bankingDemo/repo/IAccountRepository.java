package com.hg.bankingDemo.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.Customer;

/**
 * 
 * @author hernangirardi
 *
 */
@Repository
public interface IAccountRepository extends CrudRepository<Account, Long> {
	
	public Account findByOwner(Customer customer);
	
	public void deleteByOwner(Customer customer);

}

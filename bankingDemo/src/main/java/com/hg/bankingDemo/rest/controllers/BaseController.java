package com.hg.bankingDemo.rest.controllers;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.services.Impl.CustomerService;

/**
 * 
 * @author hernangirardi
 * 
 * 
 */
public class BaseController {
	
	protected final Logger log = LoggerFactory.getLogger(getClass());
	private static final String _class = "BaseController";

	@Autowired
	CustomerService customerService;
	/**
	 * 
	 * @param principal from <code>java.security.Principal</code>
	 * @return 
	 * 	 {@link #Customer} entity from <code>principal.getName()</code>
	 */
	public Customer getCurrentCustomer(Principal principal){
		if (principal == null)
			return null;
		log.info(_class + " -> getCurrentCustomer -> " + principal.getName() + "]");
		return customerService.getCustomerByUsername(principal.getName());			
	}

}

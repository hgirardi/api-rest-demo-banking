package com.hg.bankingDemo.rest.error.exceptions;

/**
 * 
 * @author hernangirardi
 * @code Custom AccountWithoutTransactionException 
 */
public class AccountWithoutTransactionException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public AccountWithoutTransactionException(String message) {
		super (message);
	}

}

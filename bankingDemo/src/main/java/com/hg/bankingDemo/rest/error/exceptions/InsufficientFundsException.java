package com.hg.bankingDemo.rest.error.exceptions;

/**
 * 
 * @author hernangirardi
 * @code Custom InsufficientFundsException 
 */
public class InsufficientFundsException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public InsufficientFundsException(double prevBalance, double amount) {
		super("Insufficient Funds:  Account Balance : " + prevBalance + " is less than " + amount);
	}
}
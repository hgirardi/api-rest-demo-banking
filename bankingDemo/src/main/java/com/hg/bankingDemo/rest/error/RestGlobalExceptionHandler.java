package com.hg.bankingDemo.rest.error;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hg.bankingDemo.rest.dto.GenericResponseJson;
import com.hg.bankingDemo.rest.dto.GenericResponseJsonImpl;
import com.hg.bankingDemo.rest.error.exceptions.AccountWithoutTransactionException;
import com.hg.bankingDemo.rest.error.exceptions.InsufficientFundsException;
import com.hg.bankingDemo.rest.error.exceptions.NoAuthorizationException;
import com.hg.bankingDemo.rest.error.exceptions.NotAllowedTransactionException;
import com.hg.bankingDemo.rest.error.exceptions.ResourceNotFoundException;
/**
 * 
 * @author hernangirardi
 * @code Main Exeption Handler
 */
@ControllerAdvice
public class RestGlobalExceptionHandler {
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public @ResponseBody GenericResponseJson handleResourceNotFound(ResourceNotFoundException ex ) {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
		HashMap<String, String> errors = new HashMap<String, String>();
		jsonResponse.setSuccess(false, "Resource not found", GenericResponseJson.RESOURCE_NOT_FOUND_MSG);
		errors.put(GenericResponseJson.DEFAULT_ERROR_TITLE_FIELD,ex.getMessage());
		jsonResponse.setErrors(errors);
		jsonResponse.setHttpResponseCode(HttpStatus.NOT_FOUND);
		return jsonResponse;
	}
	
	@ExceptionHandler(NotAllowedTransactionException.class)
	public @ResponseBody GenericResponseJson handleNotAllowedTransaction(NotAllowedTransactionException ex) {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
		HashMap<String, String> errors = new HashMap<String, String>();
		jsonResponse.setSuccess(false,"Not Allowed Transaction",ex.getMessage());
		errors.put(GenericResponseJson.DEFAULT_ERROR_TITLE_FIELD,ex.getMessage());
		jsonResponse.setErrors(errors);
		jsonResponse.setHttpResponseCode(HttpStatus.NOT_ACCEPTABLE);
		return jsonResponse;
	}
	
	@ExceptionHandler(NoAuthorizationException.class)
	public @ResponseBody GenericResponseJson handleNoAuthorization(NoAuthorizationException ex) {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
		HashMap<String, String> errors = new HashMap<String, String>();
		jsonResponse.setSuccess(false,"No Authorization Error",ex.getMessage());
		errors.put(GenericResponseJson.DEFAULT_ERROR_TITLE_FIELD,ex.getMessage());
		jsonResponse.setErrors(errors);
		jsonResponse.setHttpResponseCode(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
		return jsonResponse;
	}
	
	@ExceptionHandler(InsufficientFundsException.class)
	public @ResponseBody GenericResponseJson handleInsufficientFunds(InsufficientFundsException ex) {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
		HashMap<String, String> errors = new HashMap<String, String>();
		jsonResponse.setSuccess(false,"Insufficient Funds",ex.getMessage());
		errors.put(GenericResponseJson.DEFAULT_ERROR_TITLE_FIELD,ex.getMessage());
		jsonResponse.setErrors(errors);
		jsonResponse.setHttpResponseCode(HttpStatus.METHOD_NOT_ALLOWED);
		return jsonResponse;
	}
	
	@ExceptionHandler(AccountWithoutTransactionException.class)
	public @ResponseBody GenericResponseJson handleAccountWithoutTransaction(AccountWithoutTransactionException ex) {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
		HashMap<String, String> errors = new HashMap<String, String>();
		jsonResponse.setSuccess(false,"Account Without Transactions",ex.getMessage());
		errors.put(GenericResponseJson.DEFAULT_ERROR_TITLE_FIELD,ex.getMessage());
		jsonResponse.setErrors(errors);
		jsonResponse.setHttpResponseCode(HttpStatus.NO_CONTENT);
		return jsonResponse;
	}
	
	@ExceptionHandler(Exception.class)
	public @ResponseBody GenericResponseJson handleNotAllowedTransaction(Exception ex) {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
		HashMap<String, String> errors = new HashMap<String, String>();
		jsonResponse.setSuccess(false,"Exception",ex.getMessage());
		errors.put(GenericResponseJson.DEFAULT_ERROR_TITLE_FIELD,ex.getMessage());
		jsonResponse.setErrors(errors);
		jsonResponse.setHttpResponseCode(HttpStatus.INTERNAL_SERVER_ERROR);
		return jsonResponse;
	}
}

package com.hg.bankingDemo.rest.error.exceptions;

/**
 * 
 * @author hernangirardi
 * @code Custom NoAuthorizationException 
 */
public class NoAuthorizationException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public NoAuthorizationException(String message) {
		super(message);
	}
}
package com.hg.bankingDemo.rest.controllers;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.AccountTransaction;
import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.enums.TransactionType;
import com.hg.bankingDemo.rest.dto.AccountOperation;
import com.hg.bankingDemo.rest.dto.GenericResponseJson;
import com.hg.bankingDemo.rest.dto.GenericResponseJsonImpl;
import com.hg.bankingDemo.rest.error.exceptions.AccountWithoutTransactionException;
import com.hg.bankingDemo.rest.error.exceptions.InsufficientFundsException;
import com.hg.bankingDemo.rest.error.exceptions.NoAuthorizationException;
import com.hg.bankingDemo.rest.error.exceptions.NotAllowedTransactionException;
import com.hg.bankingDemo.rest.error.exceptions.ResourceNotFoundException;
import com.hg.bankingDemo.services.Impl.AccountService;
import com.hg.bankingDemo.services.Impl.AccountTransactionService;
/**
 * 
 * @author hernangirardi
 *
 */
@RestController
@RequestMapping(value="/account")
public class AccountController extends BaseController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	AccountTransactionService transactionService;
	
	//for validation purpose DEPOSIT LIMIT by TRANSACTION
	private static final double MAX_DEPOSIT_ON_TRANSACTION  = 3000; 
    private static final double MIN_DEPOSIT_ON_TRANSACTION = 10;
    
    /**
	 * 
	 * @param principal
	 * @return The {@link #Account} binded to current logged in user
	 */
	private Account getAccountFromCurrentUser(Principal principal) throws NoAuthorizationException{
		Customer currentCustomer = getCurrentCustomer(principal);
		if (currentCustomer == null)
			throw new NoAuthorizationException("NO AUTHORIZATION CREDENTIAL PROVIDE!");
        Account account = accountService.getAccountByCustomer(currentCustomer);
        return account;
	}
	
	@RequestMapping(value="/balance", method = RequestMethod.GET)
    public @ResponseBody GenericResponseJson getBalance(Principal principal) {
        
        GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
        Account account = getAccountFromCurrentUser(principal);
        
        if (account != null) {
            responseData.put("balance", "$"+ account.getBalance());
            jsonResponse.setSuccess(true);
            jsonResponse.setData(responseData);
            jsonResponse.setHttpResponseCode(HttpStatus.OK);
            return jsonResponse;
        } else {
        		throw new ResourceNotFoundException(principal.getName());
        }   
    }
	
	@RequestMapping(value="/deposit/{amount}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson makeDeposit(@PathVariable double amount, Principal principal) throws Exception {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
        Account account = getAccountFromCurrentUser(principal);

		if (account != null) {
			if(amount >= MIN_DEPOSIT_ON_TRANSACTION) {
				if (amount <= MAX_DEPOSIT_ON_TRANSACTION) {
					//create new DEPOSIT transaction
					AccountTransaction transaction = new AccountTransaction(TransactionType.DEPOSIT.getId(), amount, LocalDateTime.now());
					//bind the current account to the transaction
					transaction.setAccount(account);
					//persists transaction
					transactionService.save(transaction);
					//update current account balance
					double prevBalance = account.getBalance();
					double lastBalance = prevBalance + amount;
					account.setBalance(lastBalance);
					//persist new account balance
					accountService.save(account);
					jsonResponse.setSuccess(true, "Transaction Status", "Deposit sucessfully Procesed!");
					responseData.put("previos Balance", prevBalance);
					responseData.put("current Balance", account.getBalance());
					responseData.put("transaction Amount", amount);
					jsonResponse.setData(responseData);
					jsonResponse.setHttpResponseCode(HttpStatus.OK);
				} else {
					throw new NotAllowedTransactionException("The maximum amount of a deposit transaction cannot exceed 3000 EUR");
				}
			} else {
				throw new NotAllowedTransactionException("The minimum amount for a deposit transaction is 10 EUR");
			}
			return jsonResponse;	
		} else {
				throw new ResourceNotFoundException(principal.getName());
		}
	}
	
	@RequestMapping(value="/withdrawal/{amount}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson makeWithdrawal(@PathVariable double amount, Principal principal) throws Exception {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
        
		Account account = getAccountFromCurrentUser(principal);
		if (account != null) {
			double prevBalance = account.getBalance(); 				
			if(prevBalance >= amount) {
				//create new WITHDRAWAL transaction
				AccountTransaction transaction = new AccountTransaction(TransactionType.WITHDRAWAL.getId(), amount, LocalDateTime.now());
				//bind the current account to the transaction
				transaction.setAccount(account);
				//persists transaction
				transactionService.save(transaction);
				//update current account balance
				double lastBalance = prevBalance - amount;
				account.setBalance(lastBalance);
				//persist new account balance
				accountService.save(account);
				jsonResponse.setSuccess(true, "Transaction Status", "Withdrawal sucessfully Procesed!");
				responseData.put("previos Balance", prevBalance);
				responseData.put("current Balance", account.getBalance());
				responseData.put("transaction Amount", amount);
				jsonResponse.setData(responseData);
				jsonResponse.setHttpResponseCode(HttpStatus.OK);
            	return jsonResponse;
			} else {
				throw new InsufficientFundsException(prevBalance, amount);
			}
		} else {
			throw new ResourceNotFoundException(principal.getName());
		}
	}
	
	@RequestMapping(value="/statement", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson getStatement(Principal principal) {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
        Account account = getAccountFromCurrentUser(principal);

		if (account != null) {
			List<AccountTransaction> accountTransactions = transactionService.getTransactionsByAccount(account);
			if (accountTransactions.size() != 0) {
				List<AccountOperation> accountOperactions = accountTransactions.stream().map(act -> AccountOperation.fromAccountTransaction(act)).collect(Collectors.toList());
					responseData.put("Account Transactions",accountOperactions);
					jsonResponse.setData(responseData);
					jsonResponse.setSuccess(true, "Account Statements", "Account transacctional operations!");
					jsonResponse.setHttpResponseCode(HttpStatus.OK);
			} else {
				throw new AccountWithoutTransactionException("Account Without Transactional Operations");
			}
			return jsonResponse;	
		} else {
				throw new ResourceNotFoundException(principal.getName());
		}
	}
}

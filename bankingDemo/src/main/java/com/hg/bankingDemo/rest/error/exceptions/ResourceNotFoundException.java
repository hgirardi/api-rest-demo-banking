package com.hg.bankingDemo.rest.error.exceptions;

/**
 * 
 * @author hernangirardi
 * @code Custom ResourceNotFoundException 
 */
public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException(String key) {
		super("Account for UserName " + key + " NOT FOUND!");
	}
	

}

package com.hg.bankingDemo.rest.dto;

import java.util.HashMap;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author hernangirardi
 *
 */
public interface GenericResponseJson {

	static String DEFAULT_MSG_NAME_FIELD = "message";
	static String DEFAULT_MSG_TITLE_FIELD = "title";
	static String DEFAULT_MSG_TITLE_VALUE = "Internal Server Error";
	static String DEFAULT_MSG_NAME_VALUE = "The server encountered an unexpected condition which prevented it from fulfilling the request.";
	static String RESOURCE_NOT_FOUND_MSG = "The resource requested is not found. Please check your resource ID.";
	static String DEFAULT_ERROR_TITLE_FIELD = "Error Details";
	
		
	public void setSuccess(boolean success);

	public void setSuccess(boolean success, String title, String msg);

	public boolean isSuccess();

	public void setMessages(HashMap<String, String> messages);

	public HashMap<String, String> getMessages();
	
	public void setErrors(HashMap<String, String> errors);

	public HashMap<String, String> getErrors();

	public void setData(HashMap<String, Object> data);

	public HashMap<String, Object> getData();

	public void setHttpResponseCode(HttpStatus code);

	public HttpStatus getHttpResponseCode();
}
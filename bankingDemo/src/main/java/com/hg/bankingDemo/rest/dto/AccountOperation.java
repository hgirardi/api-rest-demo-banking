package com.hg.bankingDemo.rest.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.hg.bankingDemo.entity.AccountTransaction;
import com.hg.bankingDemo.enums.TransactionType;
/**
 * 
 * @author hernangirardi
 *
 */
public class AccountOperation implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String type;
	private double amount;
	private LocalDateTime date;
	
	public AccountOperation() {
		
	}
	
	public AccountOperation(Long id, String type, double amount, LocalDateTime date) {
		this.id = id;
		this.type = type;
		this.amount = amount;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	public static AccountOperation fromAccountTransaction(AccountTransaction accountTransaction) {
		String type = null;
		if (accountTransaction.getType()!=0)
			type = accountTransaction.getType() == TransactionType.DEPOSIT.getId() ? "DEPOSIT" : "WITHDRAWAL";
		return new AccountOperation(accountTransaction.getId(), type, accountTransaction.getAmount(), accountTransaction.getDate());
				
	}
	
}

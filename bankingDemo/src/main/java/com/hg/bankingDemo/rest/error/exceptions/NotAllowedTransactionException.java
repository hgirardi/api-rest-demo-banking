package com.hg.bankingDemo.rest.error.exceptions;

/**
 * 
 * @author hernangirardi
 * @code Custom NotAllowedTransactionException 
 */
public class NotAllowedTransactionException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public NotAllowedTransactionException(String invalidOperation) {
		super("Invalir Operation: " + invalidOperation);
	}

}

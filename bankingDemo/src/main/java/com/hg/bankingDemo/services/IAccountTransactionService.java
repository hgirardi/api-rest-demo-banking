package com.hg.bankingDemo.services;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.AccountTransaction;

/**
 * 
 * @author hernangirardi
 *
 */
@Transactional
public interface IAccountTransactionService {
	
	public AccountTransaction save(AccountTransaction accountTransacction) throws Exception;
	
	public AccountTransaction getTransactionById(Long IdTransaction);
	
	public List<AccountTransaction> getTransactionsByDateAndType(LocalDateTime dateStart, LocalDateTime dateEnd, int operationType);
	
	public List<AccountTransaction> getTransactionsByAccount(Account account);

}

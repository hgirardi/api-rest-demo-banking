package com.hg.bankingDemo.services.Impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.repo.IAccountRepository;
import com.hg.bankingDemo.services.IAccountService;

/**
 * 
 * @author hernangirardi
 *
 */
@Service
public class AccountService implements IAccountService {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	IAccountRepository accountRepository;

	@Override
	public Account save(Account newAccount) {
		log.info(" -> save Account -> " + newAccount.toString());
        return accountRepository.save(newAccount); 
	}

	@Override
	public Account getAccountById(Long id) {
		log.info(" -> getAccountById ID -> " + id.toString() + "]");
		return accountRepository.findById(id).orElse(new Account());
	}

	@Override
	public Account getAccountByCustomer(Customer customer) {
		log.info(" -> getAccountByCustomer Customer -> " + customer.toString() + "]");
		return accountRepository.findByOwner(customer);
	}

	@Override
	public void deleteAccountByCustomer(Customer customer) {
		log.info(" -> deleteAccountByCustomer Customer -> " + customer.toString() + "]");
		accountRepository.deleteByOwner(customer);
	}

}

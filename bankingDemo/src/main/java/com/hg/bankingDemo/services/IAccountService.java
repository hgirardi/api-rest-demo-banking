package com.hg.bankingDemo.services;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.Customer;

/**
 * 
 * @author hernangirardi
 *
 */
public interface IAccountService {

	public Account save(Account account);
	
	public Account getAccountById(Long id);
	
	public Account getAccountByCustomer(Customer customer);
	
	public void deleteAccountByCustomer(Customer customer);
	
}

package com.hg.bankingDemo.services.Impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.AccountTransaction;
import com.hg.bankingDemo.repo.IAccountTransactionRepository;
import com.hg.bankingDemo.services.IAccountTransactionService;

/**
 * 
 * @author hernangirardi
 *
 */
@Service
public class AccountTransactionService implements IAccountTransactionService {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	IAccountTransactionRepository accountTransactionRepository;
	
	@Override
	@Transactional(rollbackOn=Exception.class)
	public AccountTransaction save(AccountTransaction accountTransacction) throws Exception {
		log.info(" -> save AccountTransaction -> " + accountTransacction.toString() + "]");
		if (accountTransacction.getAmount() <= 0)
			throw new Exception("Zero or Negative Transaction not allowed");
		return accountTransactionRepository.save(accountTransacction);
	}

	@Override
	public List<AccountTransaction> getTransactionsByDateAndType(LocalDateTime dateStart, LocalDateTime dateEnd, int operationType) {
		log.info("[ -> getTransactionsByDateAndType DateStart -> " + dateStart.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.UK)) + 
				"DateEnd ->  " + dateEnd.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.UK)) + "operationType -> " + operationType + "]");
		return accountTransactionRepository.findByDateBetweenAndType(dateStart, dateEnd, operationType);
	}

	@Override
	public List<AccountTransaction> getTransactionsByAccount(Account account) {
		log.info("[ -> getTransactionsByAccount Account -> " + account.toString() + "]");
		return accountTransactionRepository.findByAccountOrderByDateDesc(account);
	}

	@Override
	public AccountTransaction getTransactionById(Long IdTransaction) {
		log.info("[ -> getTransactionById AccountTransactionId -> " + IdTransaction + "]");
		return accountTransactionRepository.findById(IdTransaction).orElse(new AccountTransaction());
	}
}

package com.hg.bankingDemo.services;

import com.hg.bankingDemo.entity.Customer;

/**
 * 
 * @author hernangirardi
 *
 */
public interface ICustomerService {
	
	public Customer save(Customer customer);
	
	public Customer getCustomerById(Long id);
	
	public Customer getCustomerByUsername(String username);

}

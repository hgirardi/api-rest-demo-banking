package com.hg.bankingDemo.services.Impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.repo.ICustomerRepository;
import com.hg.bankingDemo.services.ICustomerService;

/**
 * 
 * @author hernangirardi
 *
 */
@Service
public class CustomerService implements ICustomerService {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	ICustomerRepository customerRepository;

	@Override
	public Customer save(Customer newCustomer) {
		log.info(" -> save Customer -> " + newCustomer.toString() + "]");
        return customerRepository.save(newCustomer);
    }

	@Override
	public Customer getCustomerById(Long id) {
		log.info(" -> getCustomerById ID -> " + id.toString() + "]");
		return customerRepository.findById(id).orElse(new Customer());
	}

	@Override
	public Customer getCustomerByUsername(String username) {
		log.info(" -> getCustomerByUsername UserName -> " + username +"]");
		return customerRepository.findByUsername(username);
	}
}

package com.hg.bankingDemo.test.services;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.services.Impl.AccountService;
import com.hg.bankingDemo.services.Impl.CustomerService;

/**
 * 
 * @author hernangirardi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AccountServiceTest {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	CustomerService customerService;
	
	@Test
	public void testSaveAccount() {
		
		//create demo Customer
		Customer _customer = new Customer();
		_customer.setUsername("customerAccountTest@gmail.com");
		_customer.setPassword("demoAccountTest");
		Customer customer = customerService.save(_customer);
		
		//create accounts and bind to a demo customers
		Account _account = new Account();
		_account.setBalance(0.0);
		_account.setOwner(customer);
		Account newAccount = accountService.save(_account);
		
		assertThat(newAccount).isNotNull();
		assertThat(newAccount.getId()).isNotNull();
		assertThat(newAccount.getId()).isInstanceOf(Long.class);
		assertThat(newAccount.getBalance()).isInstanceOf(Double.class);
		assertThat(newAccount.getBalance()).isEqualTo(0.0d);
		assertThat(newAccount.getOwner()).isNotNull();
		assertThat(newAccount.getOwner()).isInstanceOf(Customer.class);
		assertThat(newAccount.getOwner().getId()).isEqualTo(customer.getId());	
	}
	
	@Test
	public void testGetAccountById() {
		
		Customer customer = customerService.getCustomerByUsername("customerAccountTest@gmail.com");
		
		//create accounts and bind to a demo customers
		Account _accountById = new Account();
		_accountById.setBalance(250.0d);
		_accountById.setOwner(customer);
		Long newAccountId = accountService.save(_accountById).getId();
		
		Account newAccountById = accountService.getAccountById(newAccountId);
		assertThat(newAccountById).isNotNull();
		assertThat(newAccountById.getId()).isNotNull();
		assertThat(newAccountById.getId()).isInstanceOf(Long.class);
		assertThat(newAccountById.getId()).isEqualTo(newAccountId);
		assertThat(newAccountById.getBalance()).isInstanceOf(Double.class);
		assertThat(newAccountById.getBalance()).isEqualTo(250.0d);
		assertThat(newAccountById.getOwner()).isNotNull();
		assertThat(newAccountById.getOwner()).isInstanceOf(Customer.class);
		assertThat(newAccountById.getOwner().getId()).isEqualTo(customer.getId());	
	}
	
	@Test
	public void testGetAccountByCustomer() {
		
		Customer _customerSingleAccount = new Customer();
		_customerSingleAccount.setUsername("customerSingleAccountTest@gmail.com");
		_customerSingleAccount.setPassword("demoAccountTest");
		Customer _customer = customerService.save(_customerSingleAccount);
				
		Account _accountByCustomer = new Account();
		_accountByCustomer.setBalance(150.90d);
		_accountByCustomer.setOwner(_customer);
		
		Account accountByCustomer = accountService.save(_accountByCustomer);
		
		Account newAccountByCustomer = accountService.getAccountByCustomer(_customer);
		assertThat(newAccountByCustomer).isNotNull();
		assertThat(newAccountByCustomer.getId()).isNotNull();
		assertThat(newAccountByCustomer.getId()).isInstanceOf(Long.class);
		assertThat(newAccountByCustomer.getId()).isEqualTo(accountByCustomer.getId());
		assertThat(newAccountByCustomer.getBalance()).isInstanceOf(Double.class);
		assertThat(newAccountByCustomer.getBalance()).isEqualTo(150.90d);
		assertThat(newAccountByCustomer.getOwner()).isNotNull();
		assertThat(newAccountByCustomer.getOwner()).isInstanceOf(Customer.class);
		assertThat(newAccountByCustomer.getOwner()).isEqualToComparingFieldByField(_customer);		
	}

}

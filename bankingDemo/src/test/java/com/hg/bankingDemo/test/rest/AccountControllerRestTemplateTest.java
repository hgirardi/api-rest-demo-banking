package com.hg.bankingDemo.test.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;
import com.hg.bankingDemo.BankingDemoApplication;
import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.AccountTransaction;
import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.rest.dto.AccountOperation;
import com.hg.bankingDemo.rest.dto.GenericResponseJson;
import com.hg.bankingDemo.rest.dto.GenericResponseJsonImpl;
import com.hg.bankingDemo.services.Impl.AccountService;
import com.hg.bankingDemo.services.Impl.AccountTransactionService;
import com.hg.bankingDemo.services.Impl.CustomerService;

/**
 * 
 * @author hernangirardi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankingDemoApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AccountControllerRestTemplateTest{
	
	@Autowired
    private MockMvc mockMvc;
	
	@Autowired
	AccountService accountservice;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	AccountTransactionService transactionService;
	
	@Test
    public void noAuthorizationExceptionTest() throws Exception {
		mockMvc.perform(get("/account/balance"))
        .andExpect(status().isUnauthorized());
    }
	
	@Test
	@WithMockUser(username="h_girardi@hotmail.com",password="her123nan")
    public void authorizationTest() throws Exception {
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
        responseData.put("balance", "$"+ 0d);
        jsonResponse.setSuccess(true);
        jsonResponse.setData(responseData);
        jsonResponse.setHttpResponseCode(HttpStatus.OK);
        
        Gson gson = new Gson();
        String json = gson.toJson(jsonResponse);
        
        Principal principal = SecurityContextHolder.getContext().getAuthentication();
		mockMvc.perform(get("/account/balance").principal(principal))
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString().equals(json.toString());
    }
	
	@Test
	@WithMockUser(username="h_girardi@hotmail.com",password="her123nan")
    public void notAllowedTransactionExceptionTest() throws Exception {
		//invalid operation
		double amount = 5;
		
		//expected respone 
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
		HashMap<String, String> errors = new HashMap<String, String>();
		jsonResponse.setSuccess(false,"Not Allowed Transaction","Invalir Operation: The minimum amount for a deposit transaction is 10 EUR");
		errors.put(GenericResponseJson.DEFAULT_ERROR_TITLE_FIELD,"Invalir Operation: The minimum amount for a deposit transaction is 10 EUR");
		jsonResponse.setErrors(errors);
		jsonResponse.setHttpResponseCode(HttpStatus.NOT_ACCEPTABLE);
		
		Gson gson = new Gson();
	    String json = gson.toJson(jsonResponse);
    	
		Principal principal = SecurityContextHolder.getContext().getAuthentication();
    	mockMvc.perform(MockMvcRequestBuilders.post("/account/deposit/{amount}",amount).principal(principal).contentType(MediaType.APPLICATION_JSON))
    			.andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().equals(json.toString());
    }
	
	
	@Test
	@WithMockUser(username="other@gmail.com",password="other")
    public void depositTest() throws Exception {
		String username = "other@gmail.com";
		Customer customerAccount = customerService.getCustomerByUsername(username);
		Account acc = accountservice.getAccountByCustomer(customerAccount);
		double amount = 150d;
		
		//expected respone 
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
		jsonResponse.setSuccess(true, "Transaction Status", "Deposit sucessfully Procesed!");
		responseData.put("previos Balance", acc.getBalance());
		responseData.put("current Balance", acc.getBalance()+amount);
		responseData.put("transaction Amount", amount);
		jsonResponse.setData(responseData);
		
		Gson gson = new Gson();
	    String json = gson.toJson(jsonResponse);
		
		Principal principal = SecurityContextHolder.getContext().getAuthentication();
    	mockMvc.perform(MockMvcRequestBuilders.post("/account/deposit/{amount}",amount).principal(principal).contentType(MediaType.APPLICATION_JSON))
    			.andDo(print())
                .andExpect(status().isOk())
    			.andReturn().getResponse().getContentAsString().equals(json.toString());
    }
	
	@Test
	@WithMockUser(username="other@gmail.com",password="other")
    public void withdrawalTest() throws Exception {
		String username = "other@gmail.com";
		Customer customerAccount = customerService.getCustomerByUsername(username);
		Account acc = accountservice.getAccountByCustomer(customerAccount);
		
		double amount = 300d;
		
		//expected respone
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
		jsonResponse.setSuccess(true, "Transaction Status", "Withdrawal sucessfully Procesed!");
		responseData.put("previos Balance", acc.getBalance());
		responseData.put("current Balance", acc.getBalance()-amount);
		responseData.put("transaction Amount", amount);
		jsonResponse.setData(responseData);
		jsonResponse.setHttpResponseCode(HttpStatus.OK);
		
    	Gson gson = new Gson();
        String json = gson.toJson(jsonResponse);
		
		Principal principal = SecurityContextHolder.getContext().getAuthentication();
		mockMvc.perform(MockMvcRequestBuilders.post("/account/withdrawal/{amount}",amount).principal(principal).contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
	        .andExpect(status().isOk())
	        .andReturn().getResponse().getContentAsString().equals(json.toString());
    }
	
	@Test
	@WithMockUser(username="other@gmail.com",password="other")
    public void statementTest() throws Exception {
		String username = "other@gmail.com";
		Customer customerAccount = customerService.getCustomerByUsername(username);
		Account acc = accountservice.getAccountByCustomer(customerAccount);
		List<AccountTransaction> accountTransactions = transactionService.getTransactionsByAccount(acc);
		
		//expected respone
		GenericResponseJson jsonResponse = new GenericResponseJsonImpl();
        HashMap<String, Object> responseData = new HashMap<>();
        List<AccountOperation> accountOperactions = accountTransactions.stream().map(act -> AccountOperation.fromAccountTransaction(act)).collect(Collectors.toList());
		responseData.put("Account Transactions",accountOperactions);
		jsonResponse.setData(responseData);
		jsonResponse.setSuccess(true, "Account Statements", "Account transacctional operations!");
		jsonResponse.setHttpResponseCode(HttpStatus.OK);

		Gson gson = new Gson();
		String json = gson.toJson(jsonResponse);
	
		Principal principal = SecurityContextHolder.getContext().getAuthentication();
		mockMvc.perform(MockMvcRequestBuilders.get("/account/statement").principal(principal).contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isOk())
			.andReturn().getResponse().getContentAsString().equals(json.toString());
    }
}

package com.hg.bankingDemo.test.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.hg.bankingDemo.entity.Account;
import com.hg.bankingDemo.entity.AccountTransaction;
import com.hg.bankingDemo.entity.Customer;
import com.hg.bankingDemo.enums.TransactionType;
import com.hg.bankingDemo.services.Impl.AccountService;
import com.hg.bankingDemo.services.Impl.AccountTransactionService;
import com.hg.bankingDemo.services.Impl.CustomerService;

/**
 * 
 * @author hernangirardi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AccountTransactionServiceTest {
	
	@Autowired 
	AccountTransactionService transactionsService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	CustomerService customerService;
	
	
	@Test
	public void testSaveTransaction() throws Exception{
		
		//create demo Customer
		Customer _customer = new Customer();
		_customer.setUsername("customerTransactionTest@gmail.com");
		_customer.setPassword("demoTransactionTest");
		Customer customer = customerService.save(_customer); //id 5L
		
		//create accounts and bind to a demo customers
		Account _account = new Account();
		_account.setBalance(0.0);
		_account.setOwner(customer);
		Account newAccount = accountService.save(_account); //id 6L
		
		AccountTransaction transaction = new AccountTransaction(TransactionType.DEPOSIT.getId(), 100, LocalDateTime.now());
		transaction.setAccount(newAccount);
		Long IdsavedTransaction = transactionsService.save(transaction).getId();
		
		AccountTransaction savedTransaction = transactionsService.getTransactionById(IdsavedTransaction);
		assertThat(savedTransaction).isNotNull();
		assertThat(savedTransaction.getId()).isEqualTo(7L);
		assertThat(savedTransaction.getType()).isEqualTo(TransactionType.DEPOSIT.getId());
		assertThat(savedTransaction.getAmount()).isEqualTo(100);
		assertThat(savedTransaction.getDate()).isBetween(LocalDate.now().atStartOfDay(), LocalDate.now().plusDays(1L).atStartOfDay());
	}
	
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();
	
	@Test
	public void testExeptionSaveTransaction() throws Exception {
		AccountTransaction transaction = new AccountTransaction(TransactionType.DEPOSIT.getId(),0L, LocalDateTime.now());
		transaction.setAccount(accountService.getAccountById(2L));
		
		exceptionRule.expect(Exception.class);
		exceptionRule.expectMessage("Zero or Negative Transaction not allowed");
		AccountTransaction notSavedTransaction = transactionsService.save(transaction);
		
		assertThat(notSavedTransaction).isNull();
	}
		
	@Test
	public void testGetTransactionsByDateAndType() throws Exception {
		
		//create demo Customer
		Customer _customer = new Customer();
		_customer.setUsername("customerTransactionDatesTest@gmail.com");
		_customer.setPassword("demoTransactionTest");
		Customer customer = customerService.save(_customer);//id 4L
		
		//create accounts and bind to a demo customers
		Account _account = new Account();
		_account.setBalance(0.0);
		_account.setOwner(customer);
		Account newAccount = accountService.save(_account);//id 5L
		
		AccountTransaction transaction = new AccountTransaction(TransactionType.DEPOSIT.getId(), 1000d, LocalDateTime.now());
		transaction.setAccount(newAccount);
		transactionsService.save(transaction);//id 6L
		
		List<AccountTransaction> transactions = transactionsService.getTransactionsByDateAndType(LocalDate.now().atStartOfDay(), LocalDate.now().plusDays(1L).atStartOfDay(), TransactionType.DEPOSIT.getId());
		//trasactions have 2 rows (id 3L and id 6L)
		assertThat(transactions.size()).isEqualTo(2);
		//get the last Transacction to compare
		assertThat(transactions.get(1)).isNotNull();
		assertThat(transactions.get(1).getType()).isEqualTo(TransactionType.DEPOSIT.getId());
		assertThat(transactions.get(1).getAmount()).isEqualTo(1000);
		assertThat(transactions.get(1).getDate()).isBetween(LocalDate.now().atStartOfDay(), LocalDate.now().plusDays(1L).atStartOfDay());
	}
	
	
	@Test
	public void testGetTransactionsByAccount() throws Exception {
		
		//create demo Customer
		Customer _customer = new Customer();
		_customer.setUsername("customerTransactionAccountTest@gmail.com");
		_customer.setPassword("demoTransactionTest");
		Customer customer = customerService.save(_customer);//id 7L
		
		//create accounts and bind to a demo customers
		Account _account = new Account();
		_account.setBalance(0.0);
		_account.setOwner(customer);
		Account newAccount = accountService.save(_account);//id 8L
		
		AccountTransaction transaction = new AccountTransaction(TransactionType.DEPOSIT.getId(), 2500d, LocalDateTime.now());
		transaction.setAccount(newAccount);
		transactionsService.save(transaction);//id 7L
		
		List<AccountTransaction> transactions = transactionsService.getTransactionsByAccount(newAccount);
		assertThat(transactions.size()).isEqualTo(1);
		assertThat(transactions.get(0)).isNotNull();
		assertThat(transactions.get(0).getType()).isEqualTo(TransactionType.DEPOSIT.getId());
		assertThat(transactions.get(0).getAmount()).isEqualTo(2500d);
		assertThat(transactions.get(0).getDate()).isBetween(LocalDate.now().atStartOfDay(), LocalDate.now().plusDays(1L).atStartOfDay());
	}
}